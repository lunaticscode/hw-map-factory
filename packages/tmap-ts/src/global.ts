type MapOptions = {
  center: Tmapv2.LatLng;
  width: string;
  height: string;
  zoom: number;
  draggable?: boolean;
  draggableSys?: boolean;
  pinchZoom?: boolean;
};

export declare namespace Tmapv2 {
  function _getScriptLocation(): string;

  export class Map {
    constructor(container: string, options: MapOptions);
  }

  export class LatLng {
    constructor(lat: number, lng: number);
    public _lat: number;
    public _lng: number;
  }

  export class Marker {
    constructor();
  }

  export class Circle {
    constructor();
  }
  export class Bounds {
    constructor();
  }
}

declare global {
  interface Window {
    Tmapv2: any;
  }
}
