import { RefObject, useEffect, useRef, useState } from "react";
const sdkScriptId = "tmap-sdk-script";
const tooltipScriptId = "tmap-tooltip-script";

type UseMapScriptArgs = {
  appKey: string;
  initLat?: number;
  initLng?: number;
  width?: string;
  height?: string;
};

const useInitMap = ({
  appKey,
  initLat = 37.566481622437934,
  initLng = 126.98502302169841,
  width = "500px",
  height = "500px",
}: UseMapScriptArgs) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<unknown>();
  const [mapRef, setMapRef] = useState<RefObject<HTMLDivElement>>({
    current: null,
  });
  const mapSdkRef = useRef<any>(null);

  const removeScript = () => {
    document.querySelector(`script#${sdkScriptId}`)?.remove();
    document.querySelector(`script#${tooltipScriptId}`)?.remove();
    mapSdkRef.current.destroy();
  };

  const appendScript = () => {
    if (document.getElementById(sdkScriptId)) return;
    setIsLoading(true);
    try {
      const script = document.createElement("script");
      script.defer = true;
      script.async = true;
      script.type = "text/javascript";
      script.setAttribute("id", sdkScriptId);
      script.src = `https://apis.openapi.sk.com/tmap/jsv2?version=1&appKey=${appKey}`;
      const tooltipeScript = document.createElement("script");
      tooltipeScript.defer = true;
      tooltipeScript.async = true;
      tooltipeScript.type = "text/javascript";
      tooltipeScript.setAttribute("id", tooltipScriptId);

      document.head.appendChild(script);

      script.onload = () => {
        const { Tmapv2 } = window;

        const tooltipSrc = Tmapv2._getScriptLocation();
        tooltipeScript.src = `${tooltipSrc}tmapjs2.min.js?version=20231206`;
        document.head.appendChild(tooltipeScript);
      };

      tooltipeScript.onload = () => {
        const { Tmapv2 } = window;

        const map = new Tmapv2.Map("map_div", {
          center: new Tmapv2.LatLng(initLat, initLng),
          width,
          height,
          zoom: 15,
        });
        mapSdkRef.current = map;
      };
    } catch (err) {
      setError(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (mapRef.current) {
      appendScript();
    }
  }, [mapRef]);

  return {
    error,
    isLoading,
    removeScript,
    setMapRef,
  };
};
export default useInitMap;
