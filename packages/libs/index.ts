export const convertDateToString = (date: Date) => {
  return date.toISOString();
};
