import { useEffect, useRef } from "react";
import "./App.css";
import { useInitMap } from "@repo/tmap-ts";

function App() {
  const mapRef = useRef(null);
  const { isLoading, setMapRef, removeScript } = useInitMap({
    appKey: import.meta.env.VITE_MAP_APP_KEY,
  });

  useEffect(() => {
    if (!document.getElementById("map_div")) {
      return;
    }
    setMapRef(mapRef);
  }, []);

  return (
    <>
      <h1>Map</h1>
      {isLoading && <h2>isLoading...</h2>}
      {!isLoading && <button onClick={removeScript}>remove</button>}
      <div ref={mapRef} id="map_div"></div>
    </>
  );
}

export default App;
